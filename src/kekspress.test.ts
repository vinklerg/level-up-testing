import { Kekspress } from './kekspress';

describe('Kekspress', () => {
  describe('getPassengers', () => {
    it('should return 0 passengers if the Kekspress is empty', () => {
      const kekspress = new Kekspress(9);
      expect(kekspress.getPassengers()).toHaveLength(0);
    });
    it('should return 1 passengers if the Kekspress has 1 passenger', () => {
      const kekspress = new Kekspress(9);
      kekspress.board('a', 1);
      expect(kekspress.getPassengers()).toHaveLength(1);
    });
  });

  describe('nextStop', () => {
    it('should remove passengers getting off', () => {
      const k = new Kekspress(9);
      k.board('a', 1);
      k.nextStop();
      expect(k.getPassengers()).toHaveLength(0);
    });
    it('should not remove passengers not yet getting off', () => {
      const k = new Kekspress(9);
      k.board('a', 2);
      k.nextStop();
      expect(k.getPassengers()).toHaveLength(1);
    });
  });

  describe('board', () => {
    it('should put on ppl boarding', () => {
      const k = new Kekspress(9);
      const boardOnce = () => k.board('a', 1);
      expect(boardOnce).not.toThrow();
    });
    it('should not let the same person board twice', () => {
      const k = new Kekspress(9);
      const boardTwice = () => {
        k.board('b', 1);
        k.board('b', 1);
      };
      expect(boardTwice).toThrow('Name b already boarded');
    });
    it('should not let more ppl board that can fit on train', () => {
      const k = new Kekspress(9);
      const boardLotOfPpl = () =>
        new Array(k.maxSeats + 1).fill('szia karesz').forEach((_, i) => k.board(i.toString(), 69));
      expect(boardLotOfPpl).not.toThrow();
    });
  });

  describe('getOff', () => {
    it('should return passanger getting off if they were on', () => {
      const k = new Kekspress(9);
      k.board('a', 1);
      const p = k.getOff('a');
      expect(p).toStrictEqual({
        name: 'a',
        getOffAt: 1,
      });
    });

    it('should return undefined if passanger wasnt on', () => {
      const k = new Kekspress(9);
      k.board('a', 1);
      const p = k.getOff('b');
      expect(p).toBeUndefined();
    });
  });
});
